FROM trion/ng-cli:8.3.8 as build

COPY . /app

RUN npm install
RUN ng build --prod --base-href=/users/

FROM nginx:1.17.4

COPY --from=build /app/dist/users-list /usr/share/nginx/html


