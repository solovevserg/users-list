import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import { AppComponent } from './components/app/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialDesignModule } from './modules/material-design/material-design.module';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserSearchComponent } from './components/user-search/user-search.component';
import { ShowMoreComponent } from './components/show-more/show-more.component';
import { UserCardComponent } from './components/user-card/user-card.component';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ReactiveFormsModule } from '@angular/forms';
import { ChooseDeclensionPipe } from './components/user-card/choose-declension.pipe';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiService } from './services/api/api.service';
import { ApiInterceptor } from './services/api/api-interceptor';
import { UsersPageComponent } from './components/users-page/users-page.component';
import { AppRoutingModule } from './modules/routing/app-routing.module';
import { InitializationService } from './services/initialization/initialization.service';
import { NetworkColorPipe } from './components/users-page/network-color.pipe';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserSearchComponent,
    ShowMoreComponent,
    UserCardComponent,
    UserInfoComponent,
    ChooseDeclensionPipe,
    UsersPageComponent,
    NetworkColorPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialDesignModule,
    ServiceWorkerModule.register('/users/ngsw-worker.js', { enabled: environment.production }),
    HttpClientModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    ApiService, {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    },
    InitializationService, {
      provide: APP_INITIALIZER,
      useFactory: (initializationService: InitializationService) => () => initializationService.init(),
      deps: [InitializationService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
