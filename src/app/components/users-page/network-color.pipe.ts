import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'networkColor'
})
export class NetworkColorPipe implements PipeTransform {

  transform(isOnline: boolean | null): string {
    
    switch (isOnline) {
      case true: return 'green';
      case false: return 'red';
      default: break;
    };
    return 'grey';
  }

}
