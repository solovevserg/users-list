import { Component, OnInit } from '@angular/core';
import { ResizeService } from '../../services/resize/resize.service';
import { ThemeService } from '../../services/theme/theme.service';
import { ConnectionService } from 'src/app/services/connection/connection.service';
import { StateService } from 'src/app/services/state/state.service';
import { Router } from '@angular/router';
import { flatMap, filter, map, first, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.css']
})
export class UsersPageComponent implements OnInit {

  sidenavOpened = true;
  sidenavType = this.resizeService.isMobile.pipe(
    map(isMobile => isMobile ? 'over' : 'side')
  );

  constructor(
    private resizeService: ResizeService,
    private themeService: ThemeService,
    public connection: ConnectionService,
    public state: StateService,
    private router: Router
  ) { }

  themeChange = this.themeService.themeChange.pipe(
    startWith(this.themeService.currentTheme),
    map(th => th === 'light')
  );

  ngOnInit() {
    this.state.selectedUserIdChange.subscribe(selectedUserId => {
      if (selectedUserId) {
        this.router.navigate(['/users', selectedUserId]);
      } else {
        this.router.navigateByUrl('/users');
      }
    });

    this.state.selectedUserIdChange.pipe(
      filter(id => id !== null),
      flatMap(id => this.resizeService.isMobile.pipe(first())),
      filter(isMobile => isMobile),
    ).subscribe(() => this.sidenavOpened = false);
  }

  onThemeChange() {
    this.themeService.toggleTheme();
  }
}
