import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['./user-search.component.css']
})
export class UserSearchComponent implements OnInit {

  @Output()
  paramsChange = new EventEmitter<any>();

  paramsForm = new FormGroup({
    query: new FormControl(''),
    verified: new FormControl(''),
    ageMin: new FormControl(''),
    ageMax: new FormControl(''),
    sort: new FormControl('name'),
    sortOrder: new FormControl('asc')
  });

  constructor() {
  }

  ngOnInit() {
    this.onParamsChange();
  }

  changeSortOrder() {
    const changedOrder = (this.paramsForm.controls['sortOrder'].value === 'asc') ? 'desc' : 'asc';
    this.paramsForm.controls['sortOrder'].setValue(changedOrder);
  }

  onParamsChange() {
    this.paramsForm.valueChanges.subscribe(value => {
      this.paramsChange.emit(value);
    });
  }
}
