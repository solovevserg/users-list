import { Component } from '@angular/core';
import { User } from 'src/app/models/user';
import { SearchService } from 'src/app/services/search/search.service';
import { StateService } from 'src/app/services/state/state.service';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent {

  constructor(
    private readonly search: SearchService,
    private readonly state: StateService,
  ) { }

  public selectedUser = this.state.selectedUserIdChange.pipe(
    map(id => id && this.state.users.find(user => user.id === id) || null)
  );

  public followers = this.selectedUser.pipe(
    filter(user => user !== null),
    map(user => this.getFollowersList(user as User))
  );

  public followersLength = this.followers.pipe(
    map(followers => followers.length)
  );

  getFollowersList({ followers }: User) {
    return this.search.search(this.state.users, {
      sort: 'name',
      sortOrder: 'asc',
      ids: followers
    });
  }
}
