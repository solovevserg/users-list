import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../../models/user';
import { StateService } from '../../services/state/state.service';
import { SearchParams } from '../../models/search-params';
import { SearchService } from '../../services/search/search.service';
import { UserSearchComponent } from '../user-search/user-search.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: User[] | null = [];

  constructor(
    private stateService: StateService,
    private searchService: SearchService
  ) { }

  ngOnInit() {
    this.onSearchQuery();
  }

  onSearchQuery(params?: SearchParams) {
    this.users = this.searchService.search(this.stateService.users, params);
  }
}
