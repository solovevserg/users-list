import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/user';
import { StateService } from '../../services/state/state.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent implements OnInit {

  @Input()
  user!: User;

  constructor(
    private readonly state: StateService,
  ) { }

  public isSelected = this.state.selectedUserIdChange.pipe(
    map(id => id === this.user.id)
  );

  ngOnInit() {
    if (!this.user) {
      throw new Error('Объект пользователя не был предоставлен при инциализации компонента');
    }
  }

  public onClick() {
    this.state.setSelectedUserId(this.user.id);
  }

}
