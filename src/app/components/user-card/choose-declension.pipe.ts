import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'chooseDeclension'
})
export class ChooseDeclensionPipe implements PipeTransform {

  transform(age: number): string {
    const prevLast = (age - age % 10) / 10 % 10;
    const last = age % 10;
    let declension = 'лет';

    if (prevLast !== 1 && last) {
      declension = (last <= 4) ? ((last === 1) ? 'год' : 'года') : 'лет';
    }

    return declension;
  }

}
