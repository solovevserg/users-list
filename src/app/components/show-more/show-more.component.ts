import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-show-more',
  templateUrl: './show-more.component.html',
  styleUrls: ['./show-more.component.css']
})

export class ShowMoreComponent implements OnInit {

  @Input()
  caption: string = 'Показать больше';

  @Output()
  isOpenedChange = new EventEmitter<boolean>();

  @Input()
  isOpened: boolean = false;

  constructor() {
  }

  ngOnInit() {
  }

  changeState() {
    this.isOpened = !this.isOpened;
    this.isOpenedChange.emit(this.isOpened);
  }
}
