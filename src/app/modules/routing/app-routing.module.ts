import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersPageComponent } from '../../components/users-page/users-page.component';
import { SelectedUserIdResolver } from 'src/app/services/url-resolver/selected-user-id.resolver';


const appRoutes: Routes = [
  {path: 'users', component: UsersPageComponent},
  {path: 'users/:id', component: UsersPageComponent, resolve: { selectedUserId: SelectedUserIdResolver }},
  {path: '**', redirectTo: '/users'}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
    SelectedUserIdResolver
  ]
})
export class AppRoutingModule { }
