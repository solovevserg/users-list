import { NgModule } from '@angular/core';

import { MatSidenavModule, MatButtonModule } from '@angular/material';
import { MatToolbarModule } from '@angular/material';
import { MatCardModule } from '@angular/material';
import { MatCheckboxModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatIconModule } from '@angular/material';

const imports = [
  MatSidenavModule,
  MatCardModule,
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatButtonToggleModule,
  MatSlideToggleModule,
  MatIconModule,
  MatToolbarModule
];

@NgModule({
  imports,
  exports: imports
})
export class MaterialDesignModule { }
