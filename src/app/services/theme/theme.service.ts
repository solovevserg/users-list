import {EventEmitter, Injectable} from '@angular/core';
import {StorageService} from '../storage/storage.service';
import {startWith} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private theme: 'light' | 'dark';
  readonly themeChange = new EventEmitter<'light' | 'dark'>();

  constructor(private readonly storageService: StorageService) {
    this.theme = this.storageService.get<'light' | 'dark'>('theme') || 'light';

    this.themeChange.pipe(startWith(this.theme)).subscribe(
        (th: 'light' | 'dark') => (th === 'light') ?
          document.body.classList.add('light-theme') :
          document.body.classList.remove('light-theme')
    );

    window.addEventListener('storage', ev => {
      if (ev.key === 'theme') {
        this.toggleTheme();
      }
    });
  }

  toggleTheme() {
    if (this.theme === 'light') {
      this.theme = 'dark';
    } else {
      this.theme = 'light';
    }
    this.storageService.add('theme', this.theme);
    this.themeChange.emit(this.theme);
  }

  get currentTheme() {
    return this.theme;
  }
}
