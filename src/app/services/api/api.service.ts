import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  private readonly USERS_URL = 'https://sdal.pw/api/cdc/users';

  constructor(private http: HttpClient) { }

  async getUsers() {
    return await this.http.get<User[]>(this.USERS_URL).toPromise();
  }
}
