import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let newReq = req;

    if (!this.isUrlAbsolute(req.url)) {
      newReq = req.clone({
        url: environment.apiOrigin + req.url
      });
    }

    return next.handle(newReq);
  }

  isUrlAbsolute(url: string): boolean {
    return /^(?:\/|[a-z]+:\/\/)/.test(url);
  }
}