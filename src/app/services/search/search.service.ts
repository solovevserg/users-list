import { Injectable } from '@angular/core';
import { User } from '../../models/user';
import { SearchParams } from '../../models/search-params';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor() { }

  search(users: User[], params: SearchParams = { sortOrder: 'asc', sort: 'name' }): User[] {
    let res = [...users];

    if (params.query) {
      const re = new RegExp('.*' + params.query.replace('.', '\\.') + '.*');
      res = res.filter(e => re.test(e.name));
    }
    // TODO: Replace false with undefined in component
    if (params.verified !== undefined && params.verified) {
      res = res.filter(e => e.verified === params.verified);
    }
    if (params.ageMax) {
      // tslint:disable-next-line:no-non-null-assertion
      res = res.filter(e => e.age <= params.ageMax!);
    }
    if (params.ageMin) {
      // tslint:disable-next-line:no-non-null-assertion
      res = res.filter(e => e.age >= params.ageMin!);
    }
    if (params.ids) {
      // tslint:disable-next-line:no-non-null-assertion
      res = res.filter(e => params.ids!.includes(e.id));
    }

    const order = (params.sortOrder === 'asc') ? 1 : -1;
    let comparator;
    switch (params.sort) {
      case 'age': comparator = (a: User, b: User) => (a.age - b.age) * order; break;
      case 'name': comparator = (a: User, b: User) => ((a.name.localeCompare(b.name))) * order; break;
      case 'subscribers': comparator = (a: User, b: User) => (a.followers.length - b.followers.length) * order;
    }
    res.sort(comparator);

    return res;
  }
}
