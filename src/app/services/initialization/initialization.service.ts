import { Injectable } from '@angular/core';
import { StateService } from '../state/state.service';

@Injectable()
export class InitializationService {

  constructor(private stateService: StateService) { }

  async init() {
    console.log('Приложение инициализировано.');
    await this.stateService.init();
  }
}
