import { Injectable } from '@angular/core';
import { User } from '../../models/user';
import { StorageService } from '../storage/storage.service';
import { ApiService } from '../api/api.service';
import { environment } from '../../../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, startWith } from 'rxjs/operators';
import { SearchParams } from 'src/app/models/search-params';

@Injectable({
  providedIn: 'root'
})
export class StateService {
  private _users: User[] | null = null;
  private readonly USERS_KEY = 'usersList';
  private readonly USERS_FETCH_TIMESTAMP_KEY = 'usersFetchTimestamp';
  private readonly _selectedUserIdObservable = new BehaviorSubject<number | null>(null);

  constructor(
    private storageService: StorageService,
    private apiService: ApiService
  ) { }

  async init() {
    this._users = this.storageService.get(this.USERS_KEY);
    const _usersFetchTimestamp: number | null = this.storageService.get(this.USERS_FETCH_TIMESTAMP_KEY);
    if (this._users === null || _usersFetchTimestamp === null || _usersFetchTimestamp + environment.dataLifetime < Date.now()) {
      await this.fetchUsers();
    }
  }

  private async fetchUsers() {
    this._users = await this.apiService.getUsers();
    this.storageService.add(this.USERS_KEY, this._users);
    this.storageService.add(this.USERS_FETCH_TIMESTAMP_KEY, Date.now());
  }

  get users() {
    return this._users || [];
  }


  readonly selectedUserIdChange = this._selectedUserIdObservable.pipe(
      distinctUntilChanged(),
  );
  
  setSelectedUserId(userId: number | null) {
    this._selectedUserIdObservable.next(userId);
  }
}
