import { Injectable } from '@angular/core';
import { fromEvent } from 'rxjs';
import { throttleTime, map, startWith } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ResizeService {
  private readonly WINDOW_MOBILE_SIZE = 900;

  public windowSize = fromEvent(window, 'resize').pipe(
    throttleTime(200)
  );
  public isMobile = this.windowSize.pipe(
    startWith(window.innerWidth <= this.WINDOW_MOBILE_SIZE),
    map(() => (window.innerWidth <= this.WINDOW_MOBILE_SIZE))
  );

  constructor() { }
}
