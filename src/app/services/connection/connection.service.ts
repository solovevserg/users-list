import { Injectable } from '@angular/core';
import { merge, fromEvent, Observable } from 'rxjs';
import { startWith, mapTo } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  private _isOnline: Observable<boolean> = merge(
    fromEvent(window, 'online').pipe(mapTo(true)),
    fromEvent(window, 'offline').pipe(mapTo(false))
  );

  public isOnline: Observable<boolean | null> = this._isOnline.pipe(
    startWith((navigator.onLine === undefined) ? null : navigator.onLine)
  );

  constructor() { }
}
