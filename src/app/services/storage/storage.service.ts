import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class StorageService {

  constructor() { }

  get<T>(key: string): T | null {
    const value = localStorage.getItem(key);
    return value && JSON.parse(value);
  }

  add<T>(key:string, obj: T) {
    const value = JSON.stringify(obj);
    localStorage.setItem(key, value);
  }

  includes(key: string): boolean {
    return (localStorage.getItem(key)) !== null;
  }

  remove(key:string) {
    localStorage.removeItem(key);
  }
}
