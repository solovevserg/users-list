import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { StateService } from 'src/app/services/state/state.service';

@Injectable()
export class SelectedUserIdResolver implements Resolve<number> {

  constructor(private state: StateService) { }

  resolve(route: ActivatedRouteSnapshot): number {

    const selectedUserId: number = Number(route.paramMap.get('id'));

    this.state.setSelectedUserId(selectedUserId);
    return selectedUserId;
  }
  
}