export interface User {
    id: number;
    name: string;
    verified: boolean;
    age: number;
    followers: number[];
}