export interface SearchParams {
    query?: string;
    verified?: boolean;
    ageMin?: number;
    ageMax?: number;
    sort: 'name' | 'age' | 'subscribers';
    sortOrder: 'desc' | 'asc';
    ids?: number[];
}
