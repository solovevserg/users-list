export const environment = {
  production: false,
  apiOrigin: 'http://127.0.0.1:5000/',
  dataLifetime: 86400000
};
